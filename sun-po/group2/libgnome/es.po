# Translation into spanish of libgnome.
# Copyright (C) 2001 Free Software Foundation, Inc.
# Héctor García Álvarez <hector@scouts-es.org>, 2001-2002.
#
msgid ""
msgstr ""
"Project-Id-Version: libgnome2 1.100.0\n"
"POT-Creation-Date: 2002-07-24 12:45+0100\n"
"PO-Revision-Date: 2002-07-24 12:47+0100\n"
"Last-Translator: Sun G11n <gnome_int_l10n@ireland.sun.com>\n"
"Language-Team: Sun G11n <gnome_int_l10n@ireland.sun.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: gnome-data/gnome-2.soundlist.in.h:1
msgid "Error Messages"
msgstr "Mensajes de error"

#: gnome-data/gnome-2.soundlist.in.h:2
# SUN CHANGED MESSAGE
msgid "GNOME system events"
msgstr "Eventos del sistema GNOME"


#: gnome-data/gnome-2.soundlist.in.h:3
msgid "Informational Messages"
msgstr "Mensajes informativos"

#: gnome-data/gnome-2.soundlist.in.h:4
msgid "Login"
msgstr "Apertura de sesión"

#: gnome-data/gnome-2.soundlist.in.h:5
msgid "Logout"
msgstr "Salida de sesión"

#: gnome-data/gnome-2.soundlist.in.h:6
msgid "Miscellaneous Messages"
msgstr "Mensajes misceláneos"

#: gnome-data/gnome-2.soundlist.in.h:7
msgid "Question Dialogs"
msgstr "Diálogos con preguntas"

#: gnome-data/gnome-2.soundlist.in.h:8
msgid "Warning Messages"
msgstr "Mensajes de advertencia"

#: gnome-data/gtk-events-2.soundlist.in.h:1
msgid "Action button click"
msgstr "Acción de clic del ratón"

#: gnome-data/gtk-events-2.soundlist.in.h:2
msgid "Check box toggled"
msgstr "Cambio de una \"checkbox\""

#: gnome-data/gtk-events-2.soundlist.in.h:3
msgid "Menu item activation"
msgstr "Activación de un elemento de menú"

#: gnome-data/gtk-events-2.soundlist.in.h:4
# SUN CHANGED MESSAGE
msgid "User interface events"
msgstr "Eventos de la interfaz del usuario"


#: libgnome/gnome-exec.c:438
# SUN CHANGED MESSAGE
msgid "Cannot find a terminal, using xterm, even if it may not work"
msgstr ""
"No se pudo encontrar un terminal, usando xterm, incluso aunque no funcione"


#: libgnome/gnome-gconf.c:174
msgid "GNOME GConf Support"
msgstr "Soporte para GNOME GConf"

#: libgnome/gnome-help.c:167
# SUN CHANGED MESSAGE
msgid "Unable to find the GNOME_FILE_DOMAIN_APP_HELP domain"
msgstr "No se pudo encontrar el dominio GNOME_FILE_DOMAIN_APP_HELP"


#: libgnome/gnome-help.c:180
# SUN CHANGED MESSAGE
msgid "Unable to find the GNOME_FILE_DOMAIN_HELP domain."
msgstr "No se pudo encontrar el dominio GNOME_FILE_DOMAIN_HELP."


#: libgnome/gnome-help.c:193 libgnome/gnome-help.c:208
#, c-format
# SUN CHANGED MESSAGE
msgid ""
"Unable to show help as %s is not a directory.  Please check your "
"installation."
msgstr ""
"No se pudo mostrar la ayuda ya que %s no es un directorio. Compruebe su "
"instalación."


#: libgnome/gnome-help.c:217 libgnome/gnome-help.c:233
#, c-format
# SUN CHANGED MESSAGE
msgid ""
"Unable to find the help files in either %s or %s.  Please check your "
"installation"
msgstr ""
"No se pudo encontrar los archivos de ayuda en %s ni en %s. Compruebe su "
"instalación."


#: libgnome/gnome-help.c:312
#, c-format
# SUN CHANGED MESSAGE
msgid "Unable to find doc_id %s in the help path"
msgstr "No se pudo encontrar doc_id %s en la ruta de la ayuda"


#: libgnome/gnome-help.c:333
#, c-format
# SUN CHANGED MESSAGE
msgid "Help document %s/%s not found"
msgstr "No se encontró el documento de ayuda %s/%s"


#: libgnome/gnome-i18n.c:85
# SUN CHANGED MESSAGE
msgid "Too many alias levels for a locale, may indicate a loop"
msgstr "Demasiados niveles de alias para un locale puede indicar un bucle"


#. FIXME: get this from bonobo
#: libgnome/gnome-init.c:88
msgid "Bonobo Support"
msgstr "Soporte para Bonobo"

#: libgnome/gnome-init.c:142
msgid "Bonobo activation Support"
msgstr "Soporte para activación de bonobo"

#: libgnome/gnome-init.c:281 libgnome/gnome-init.c:295
#, c-format
# SUN CHANGED MESSAGE
msgid "Could not create per-user gnome configuration directory `%s': %s\n"
msgstr ""
"No se pudo crear un directorio para la configuración de usuario '%s': %s\n"


#: libgnome/gnome-init.c:304
#, c-format
# SUN CHANGED MESSAGE
msgid ""
"Could not set mode 0700 on private per-user gnome configuration directory `%"
"s': %s\n"
msgstr ""
"No se pudo poner el modo 0700 al directorio de configuración de usuario %s: %s\n"


#: libgnome/gnome-init.c:311
#, c-format
# SUN CHANGED MESSAGE
msgid "Could not create gnome accelerators directory `%s': %s\n"
msgstr "No se pudo crear el directorio de aceleradores gnome `%s': %s\n"


#: libgnome/gnome-init.c:367
msgid "Disable sound server usage"
msgstr "Deshabilitar el uso del servidor de sonido"

#: libgnome/gnome-init.c:370
msgid "Enable sound server usage"
msgstr "Habilitar el uso del servidor de sonido"

#: libgnome/gnome-init.c:373
# SUN CHANGED MESSAGE
msgid "Host:port on which the sound server to use is running"
msgstr "Ordenador:puerto en el que se está ejecutando el servidor de sonido"


#: libgnome/gnome-init.c:375
msgid "HOSTNAME:PORT"
msgstr "ORDENADOR:PUERTO"

#: libgnome/gnome-init.c:393
msgid "GNOME Virtual Filesystem"
msgstr "Sistema de archivos virtual de GNOME"

#: libgnome/gnome-init.c:414
msgid "GNOME Library"
msgstr "Librería de GNOME"

#: libgnome/gnome-program.c:426
msgid "Popt Table"
msgstr "Tabla popt"

#: libgnome/gnome-program.c:427
msgid "The table of options for popt"
msgstr "La tabla de opciones para popt"

#: libgnome/gnome-program.c:434
msgid "Popt Flags"
msgstr "Opciones popt"

#: libgnome/gnome-program.c:435
# SUN CHANGED MESSAGE
msgid "The flags to use for popt"
msgstr "Las opciones que usar para popt"


#: libgnome/gnome-program.c:443
msgid "Popt Context"
msgstr "Contexto popt"

#: libgnome/gnome-program.c:444
# SUN CHANGED MESSAGE
msgid "The popt context pointer that GnomeProgram is using"
msgstr "El puntero del contexto popt que el programa Gnome está usando"


#: libgnome/gnome-program.c:452
# SUN CHANGED MESSAGE
msgid "Human readable name"
msgstr "Nombre leíble por humanos"


#: libgnome/gnome-program.c:453
# SUN CHANGED MESSAGE
msgid "Human readable name of this application"
msgstr "Nombre leíble por humanos de esta aplicación"


#: libgnome/gnome-program.c:462
msgid "GNOME path"
msgstr "Path GNOME"

#: libgnome/gnome-program.c:463
msgid "Path in which to look for installed files"
msgstr "Path en el que buscar archivos instalados"

#: libgnome/gnome-program.c:472
msgid "App ID"
msgstr "App ID"

#: libgnome/gnome-program.c:473
# SUN CHANGED MESSAGE
msgid "ID string to use for this application"
msgstr "Cadena ID que usar para esta aplicación"


#: libgnome/gnome-program.c:480
msgid "App version"
msgstr "Versión de la aplicación"

#: libgnome/gnome-program.c:481
msgid "Version of this application"
msgstr "Versión de esta aplicación"

#: libgnome/gnome-program.c:488
msgid "GNOME Prefix"
msgstr "Prefijo GNOME"

#: libgnome/gnome-program.c:489
msgid "Prefix where GNOME was installed"
msgstr "Directorio donde se instaló GNOME"

#: libgnome/gnome-program.c:498
msgid "GNOME Libdir"
msgstr "Directorio de la librería de GNOME"

#: libgnome/gnome-program.c:499
# SUN CHANGED MESSAGE
msgid "Library prefix where GNOME was installed"
msgstr "Directorio donde se instalaron las librerías de GNOME"


#: libgnome/gnome-program.c:508
msgid "GNOME Datadir"
msgstr "Directorio de datos de GNOME"

#: libgnome/gnome-program.c:509
msgid "Data prefix where GNOME was installed"
msgstr "Directorio de datos donde se instaló GNOME"

#: libgnome/gnome-program.c:518
msgid "GNOME Sysconfdir"
msgstr "Directorio de configuración de GNOME"

#: libgnome/gnome-program.c:519
msgid "Configuration prefix where GNOME was installed"
msgstr "Directorio de configuración donde se instaló GNOME"

#: libgnome/gnome-program.c:529
# SUN CHANGED MESSAGE
msgid "GNOME App Prefix"
msgstr "Directorio de aplicaciones GNOME"


#: libgnome/gnome-program.c:530
msgid "Prefix where this application was installed"
msgstr "Directorio donde se instaló esta aplicación"

#: libgnome/gnome-program.c:538
msgid "GNOME App Libdir"
msgstr "Directorio de librerías de aplicaciones de GNOME"

#: libgnome/gnome-program.c:539
msgid "Library prefix where this application was installed"
msgstr "Directorio de librerías donde se instaló esta aplicación"

#: libgnome/gnome-program.c:548
msgid "GNOME App Datadir"
msgstr "Directorio de datos de aplicaciones GNOME"

#: libgnome/gnome-program.c:549
# SUN CHANGED MESSAGE
msgid "Data prefix where this application was installed"
msgstr "Directorio de datos donde se instaló esta aplicación"


#: libgnome/gnome-program.c:558
msgid "GNOME App Sysconfdir"
msgstr "Directorio de configuración de aplicaciones GNOME"

#: libgnome/gnome-program.c:559
msgid "Configuration prefix where this application was installed"
msgstr "Directorio de configuración donde se instaló esta aplicación"

#: libgnome/gnome-program.c:568
msgid "Create Directories"
msgstr "Crear directorios"

#: libgnome/gnome-program.c:569
# SUN CHANGED MESSAGE
msgid "Create standard GNOME directories on startup"
msgstr "Crear directorios estándar de GNOME al iniciar"


#: libgnome/gnome-program.c:578
msgid "Enable Sound"
msgstr "Habilitar sonido"

#: libgnome/gnome-program.c:579
# SUN CHANGED MESSAGE
msgid "Enable sound on startup"
msgstr "Habilitar el servidor de sonido al iniciar"


#: libgnome/gnome-program.c:588
msgid "Espeaker"
msgstr "Altavoz"

#: libgnome/gnome-program.c:589
# SUN CHANGED MESSAGE
msgid "How to connect to esd"
msgstr "Cómo conectarse con esd"


#: libgnome/gnome-program.c:1352
msgid "Help options"
msgstr "Opción de ayuda"

#: libgnome/gnome-program.c:1357
msgid "Application options"
msgstr "Opciones de la aplicación"

#: libgnome/gnome-program.c:1373
# SUN CHANGED MESSAGE
msgid "Dynamic modules to load"
msgstr "Módulos dinámicos que cargar"


#: libgnome/gnome-program.c:1374
msgid "MODULE1,MODULE2,..."
msgstr "MODULE1,MODULE2,..."

#: monikers/GNOME_Moniker_std.server.in.in.h:1
msgid "Extra Moniker factory"
msgstr "Extra Moniker factory"

#: monikers/GNOME_Moniker_std.server.in.in.h:2
msgid "GConf moniker"
msgstr "GConf moniker"

#: monikers/GNOME_Moniker_std.server.in.in.h:3
msgid "config indirect moniker"
msgstr "moniker de configuración indirecta"

#: monikers/bonobo-config-bag.c:230
msgid "Unknown type"
msgstr "Tipo desconocido"

#: monikers/bonobo-moniker-conf-indirect.c:44
#, c-format
msgid "Key %s not found in configuration"
msgstr "La clave %s no se encontró en la configuración"
