# Brazilian Translation of Eazel Extensions Library.
# Copyright (C) 2001 Free Software Foundation, Inc.
# Gustavo Maciel Dias Vieira <gdvieira@zaz.com.br>, 2001.
#
msgid ""
msgstr ""
"Project-Id-Version: eel 1.0.2\n"
"POT-Creation-Date: 2002-08-07 14:58+0100\n"
"PO-Revision-Date: 2002-08-07 03:01+0100\n"
"Last-Translator: Sun G11n <gnome_int_l10n@ireland.sun.com>\n"
"Language-Team: Sun G11n <gnome_int_l10n@ireland.sun.com>\n"
"BR <debian-l10n-portuguese@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: eel/eel-gconf-extensions.c:83
#, c-format
msgid ""
"GConf error:\n"
"  %s"
msgstr ""
"Erro do GConf:\n"
"  %s"

#: eel/eel-gconf-extensions.c:87
#, c-format
msgid ""
"GConf error:\n"
"  %s\n"
"All further errors shown only on terminal"
msgstr ""
"Erro do GConf:\n"
"  %s\n"
"Todos os erros subseqüentes exibidos apenas no terminal"

#: eel/eel-gconf-extensions.c:91
msgid "GConf Error"
msgstr "Erro do GConf"

#. localizers: These strings are part of the strftime
#. * self-check code and must be changed to match what strtfime
#. * yields. The first one is "%m/%d/%y, %I:%M %p".
#.
#: eel/eel-glib-extensions.c:1128
msgid "01/01/00, 01:00 AM"
msgstr "01/01/00, 01:00"

#. The second one is "%-m/%-d/%-y, %-I:%M %p".
#: eel/eel-glib-extensions.c:1130
msgid "1/1/00, 1:00 AM"
msgstr "1/1/00, 1:00"

#. The third one is "%_m/%_d/%_y, %_I:%M %p".
#: eel/eel-glib-extensions.c:1132
msgid " 1/ 1/00,  1:00 AM"
msgstr " 1/ 1/00,  1:00"

#: eel/eel-gnome-extensions.c:611
msgid "No image was selected.  You must click on an image to select it."
msgstr ""
"Nenhuma imagem foi selecionada. Você deve clicar em uma imagem para "
"selecioná-la."

#: eel/eel-gnome-extensions.c:612
msgid "No selection made"
msgstr "Nenhuma seleção foi feita"

#: eel/eel-password-dialog.c:203
msgid "_Username:"
msgstr "Nome do _Usuário:"

#: eel/eel-password-dialog.c:210
msgid "_Password:"
msgstr "_Senha:"

#: eel/eel-password-dialog.c:237
msgid "Remember this password"
msgstr "Lembrar esta senha"

#: eel/eel-stock-dialogs.c:457 eel/eel-stock-dialogs.c:490
msgid "Info"
msgstr "Informação"

#: eel/eel-stock-dialogs.c:492 eel/eel-stock-dialogs.c:550
msgid "Details"
msgstr "Detalhes"

#: eel/eel-stock-dialogs.c:515
msgid "Warning"
msgstr "Alerta"

#: eel/eel-stock-dialogs.c:526 eel/eel-stock-dialogs.c:548
msgid "Error"
msgstr "Erro"

#: eel/eel-stock-dialogs.c:584 eel/eel-stock-dialogs.c:617
msgid "Question"
msgstr "Pergunta"

#: eel/eel-vfs-extensions.c:687
msgid " (invalid Unicode)"
msgstr "(Unicode inválido)"

#, fuzzy
#~ msgid "position"
#~ msgstr "Pergunta"

#~ msgid "_Move here"
#~ msgstr "_Mover aqui"

#~ msgid "_Copy here"
#~ msgstr "_Copiar aqui"

#~ msgid "_Link here"
#~ msgstr "_Vincular aqui"

#~ msgid "Cancel"
#~ msgstr "Cancelar"

#~ msgid "Normal"
#~ msgstr "Normal"

#~ msgid "Unknown"
#~ msgstr "Desconhecido"

#~ msgid "Bold"
#~ msgstr "Negrito"

#~ msgid "Book"
#~ msgstr "Livro"

#~ msgid "Black"
#~ msgstr "Preto"

#~ msgid "Demibold"
#~ msgstr "Meionegrito"

#~ msgid "Light"
#~ msgstr "Leve"

#~ msgid "Italic"
#~ msgstr "Itálico"

#~ msgid "Oblique"
#~ msgstr "Oblíquo"

#~ msgid "Reverse Italic"
#~ msgstr "Itálico invertido"

#~ msgid "Reverse Oblique"
#~ msgstr "Oblíquo invertido"

#~ msgid "Other"
#~ msgstr "Outro"

#~ msgid "Condensed"
#~ msgstr "Condensado"

#~ msgid "Semicondensed"
#~ msgstr "Semicondensado"

#~ msgid "More..."
#~ msgstr "Mais..."

#~ msgid " -_,;.?/&"
#~ msgstr " -_,;.?/&"

#~ msgid "Beginner"
#~ msgstr "Novato"

#~ msgid "Intermediate"
#~ msgstr "Intermediário"

#~ msgid "Advanced"
#~ msgstr "Avançado"
