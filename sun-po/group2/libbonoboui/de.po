# German translation for libbonoboui.
# Copyright (C) 2002 Free Software Foundation, Inc.
# Jrgen Scheibengruber, <mfcn@gmx.de>, 2002.
#
msgid ""
msgstr ""
"Project-Id-Version: libbonoboui (cvs)\n"
"POT-Creation-Date: 2002-07-24 11:03+0100\n"
"PO-Revision-Date: 2002-07-24 11:06+0100\n"
"Last-Translator: Sun G11n <gnome_int_l10n@ireland.sun.com>\n"
"Language-Team: Sun G11n <gnome_int_l10n@ireland.sun.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.5\n"

#: bonobo/bonobo-canvas-item.c:647
msgid "corba factory"
msgstr "CORBA-Fabrik"

#: bonobo/bonobo-canvas-item.c:648
# SUN CHANGED MESSAGE
msgid "The factory pointer"
msgstr "Der Fabrikzeiger"


#: bonobo/bonobo-canvas-item.c:657
msgid "corba UI container"
msgstr "CORBA-UI-Container"

#: bonobo/bonobo-canvas-item.c:658
# SUN CHANGED MESSAGE
msgid "The User interface container"
msgstr "Der BenutzeroberflÃ¤chen-Container"


#: bonobo/bonobo-dock-item-grip.c:236
#, fuzzy
# SUN CHANGED MESSAGE
msgid "Dock the toolbar"
msgstr "Verankern der Symbolleiste"


#: bonobo/bonobo-dock-item-grip.c:237
#, fuzzy
# SUN CHANGED MESSAGE
msgid "Un dock the toolbar"
msgstr "Freistellen der Symbolleiste"


#: bonobo/bonobo-dock-item.c:291 bonobo/bonobo-dock-item.c:292
# SUN CHANGED MESSAGE
msgid "Shadow type"
msgstr "Schattentyp"


#: bonobo/bonobo-dock-item.c:302 bonobo/bonobo-dock-item.c:303
#: bonobo/bonobo-ui-toolbar.c:1085 bonobo/bonobo-ui-toolbar.c:1086
# SUN CHANGED MESSAGE
msgid "Orientation"
msgstr "Ausrichtung"


#: bonobo/bonobo-dock-item.c:313 bonobo/bonobo-dock-item.c:314
#: bonobo/bonobo-ui-toolbar.c:1104 bonobo/bonobo-ui-toolbar.c:1105
msgid "Preferred width"
msgstr "Bevorzugte Breite"

#: bonobo/bonobo-dock-item.c:322 bonobo/bonobo-dock-item.c:323
#: bonobo/bonobo-ui-toolbar.c:1113 bonobo/bonobo-ui-toolbar.c:1114
# SUN CHANGED MESSAGE
msgid "Preferred height"
msgstr "Bevorzugte HÃ¶he"


#: bonobo/bonobo-file-selector-util.c:383
# SUN CHANGED MESSAGE
msgid "Select a file to open"
msgstr "WÃ¤hlen Sie eine Datei zum Ãffnen aus."


#: bonobo/bonobo-file-selector-util.c:411
# SUN CHANGED MESSAGE
msgid "Select files to open"
msgstr "WÃ¤hlen Sie Dateien zum Ãffnen aus."


#: bonobo/bonobo-file-selector-util.c:440
# SUN CHANGED MESSAGE
msgid "Select a filename to save"
msgstr "WÃ¤hlen Sie einen Dateinamen zum Speichern aus."


#: bonobo/bonobo-plug.c:358
msgid "Event Forwarding"
msgstr "Ereignis-Weiterleitung"

#: bonobo/bonobo-plug.c:359
msgid "Whether X events should be forwarded"
msgstr "Ob X-Ereignis weitergeleitet werden sollen"

#: bonobo/bonobo-selector-widget.c:300 bonobo/bonobo-window.c:447
msgid "Name"
msgstr "Name"

#: bonobo/bonobo-selector-widget.c:322
msgid "Description"
msgstr "Beschreibung"

#: bonobo/bonobo-selector.c:345
# SUN NEW TRANSLATION
msgid "Interfaces required"
msgstr "BenÃ¶tigte Schnittstellen"


#: bonobo/bonobo-selector.c:346
# SUN NEW TRANSLATION
msgid ""
"A NULL_terminated array of interfaces which a server must support in order "
"to be listed in the selector.  Defaults to \"IDL:Bonobo/Embeddable:1.0\" if "
"no interfaces are listed"
msgstr ""
"Ein mit NULL abgeschlossener Array von Schnittstellen, die ein Server unterstÃ¼tzen muss, um  "
"in der Auswahl aufgefÃ¼hrt zu werden. Standardeinstellung fÃ¼r \"IDL:Bonobo/Embeddable:1.0\" wenn "
"keine Schnittstellen aufgelistet sind."


#: bonobo/bonobo-selector.c:348
# SUN NEW TRANSLATION
msgid "Interface required entry"
msgstr "FÃ¼r Schnittstelle erforderlicher Eintrag"


#: bonobo/bonobo-selector.c:349
# SUN NEW TRANSLATION
msgid "One of the interfaces that's required"
msgstr "Eine der benÃ¶tigten Schnittstellen"


#: bonobo/bonobo-ui-config-widget.c:267
msgid "Visible"
msgstr "Sichtbar"

#: bonobo/bonobo-ui-config-widget.c:274
msgid "_Show"
msgstr "_Zeigen"

#: bonobo/bonobo-ui-config-widget.c:281
msgid "_Hide"
msgstr "_Verbergen"

#: bonobo/bonobo-ui-config-widget.c:287
# SUN CHANGED MESSAGE
msgid "_View tooltips"
msgstr "Minihilfen _anzeigen"


#: bonobo/bonobo-ui-config-widget.c:292
# SUN CHANGED MESSAGE
msgid "Toolbars"
msgstr "Symbolleisten"


#: bonobo/bonobo-ui-config-widget.c:302
# SUN CHANGED MESSAGE
msgid "toolbars"
msgstr "Symbolleisten"


#: bonobo/bonobo-ui-config-widget.c:313 bonobo/bonobo-ui-sync-toolbar.c:556
# SUN CHANGED MESSAGE
msgid "Look"
msgstr "Erscheinungsbild"


#: bonobo/bonobo-ui-config-widget.c:322 bonobo/bonobo-ui-sync-toolbar.c:556
# SUN CHANGED MESSAGE
msgid "_Icon"
msgstr "_Symbol"


#: bonobo/bonobo-ui-config-widget.c:328
# SUN CHANGED MESSAGE
msgid "_Text and Icon"
msgstr "_Text und Symbol"


#: bonobo/bonobo-ui-config-widget.c:334
#, fuzzy
# SUN CHANGED MESSAGE
msgid "Text only"
msgstr "Nur Text"


#: bonobo/bonobo-ui-config-widget.c:340
# SUN CHANGED MESSAGE
msgid "_Priority text only"
msgstr "_PrioritÃ¤t nur Text"


#: bonobo/bonobo-ui-engine-config.c:521
msgid "Configure UI"
msgstr "UI konfigurieren"

#: bonobo/bonobo-ui-init-gtk.c:113
# SUN NEW TRANSLATION
msgid "Gdk debugging flags to set"
msgstr "Gdk-Diagnoseflags zum Festlegen"


#: bonobo/bonobo-ui-init-gtk.c:113 bonobo/bonobo-ui-init-gtk.c:116
#: bonobo/bonobo-ui-init-gtk.c:162 bonobo/bonobo-ui-init-gtk.c:165
# SUN NEW TRANSLATION
msgid "FLAGS"
msgstr "FLAGS"


#: bonobo/bonobo-ui-init-gtk.c:116
# SUN NEW TRANSLATION
msgid "Gdk debugging flags to unset"
msgstr "Gdk-Diagnoseflags zum ZurÃ¼cksetzen"


#: bonobo/bonobo-ui-init-gtk.c:120
# SUN NEW TRANSLATION
msgid "X display to use"
msgstr "Zu verwendende X-Anzeige"


#: bonobo/bonobo-ui-init-gtk.c:120
# SUN NEW TRANSLATION
msgid "DISPLAY"
msgstr "ANZEIGE"


#: bonobo/bonobo-ui-init-gtk.c:125
# SUN NEW TRANSLATION
msgid "X screen to use"
msgstr "Zu verwendender X-Bildschirm"


#: bonobo/bonobo-ui-init-gtk.c:125
# SUN NEW TRANSLATION
msgid "SCREEN"
msgstr "BILDSCHIRM"


#: bonobo/bonobo-ui-init-gtk.c:129
# SUN NEW TRANSLATION
msgid "Make X calls synchronous"
msgstr "Synchronisieren von X-Anrufen"


#: bonobo/bonobo-ui-init-gtk.c:135
# SUN NEW TRANSLATION
msgid "Don't use X shared memory extension"
msgstr "Die von X freigegebene Speichererweiterung nicht verwenden"


#: bonobo/bonobo-ui-init-gtk.c:139
# SUN NEW TRANSLATION
msgid "Program name as used by the window manager"
msgstr "Programmname wie von Fenster-Manager"


#: bonobo/bonobo-ui-init-gtk.c:139
# SUN NEW TRANSLATION
msgid "NAME"
msgstr "NAME"


#: bonobo/bonobo-ui-init-gtk.c:142
# SUN NEW TRANSLATION
msgid "Program class as used by the window manager"
msgstr "Programmklasse wie von Fenster-Manager"


#: bonobo/bonobo-ui-init-gtk.c:142
# SUN NEW TRANSLATION
msgid "CLASS"
msgstr "KLASSE"


#: bonobo/bonobo-ui-init-gtk.c:146
# SUN NEW TRANSLATION
msgid "HOST"
msgstr "HOST"


#: bonobo/bonobo-ui-init-gtk.c:150
# SUN NEW TRANSLATION
msgid "PORT"
msgstr "ANSCHLUSS"


#: bonobo/bonobo-ui-init-gtk.c:155 bonobo/bonobo-ui-init-gtk.c:158
# SUN NEW TRANSLATION
msgid "STYLE"
msgstr "STIL"


#: bonobo/bonobo-ui-init-gtk.c:162
# SUN NEW TRANSLATION
msgid "Gtk+ debugging flags to set"
msgstr "Gtk+-Diagnoseflags zu Festlegen"


#: bonobo/bonobo-ui-init-gtk.c:165
# SUN NEW TRANSLATION
msgid "Gtk+ debugging flags to unset"
msgstr "Gtk+-Diagnoseflags zum ZurÃ¼cksetzen"


#: bonobo/bonobo-ui-init-gtk.c:168
# SUN NEW TRANSLATION
msgid "Make all warnings fatal"
msgstr "Warnungen als schwerwiegend festlegen"


#: bonobo/bonobo-ui-init-gtk.c:171
# SUN NEW TRANSLATION
msgid "Load an additional Gtk module"
msgstr "ZusÃ¤tzliches Gtk-Modul laden"


#: bonobo/bonobo-ui-init-gtk.c:171
# SUN NEW TRANSLATION
msgid "MODULE"
msgstr "MODUL"


#: bonobo/bonobo-ui-init-gtk.c:180
# SUN NEW TRANSLATION
msgid "GTK+"
msgstr "GTK+"


#: bonobo/bonobo-ui-main.c:188
# SUN NEW TRANSLATION
msgid "Bonobo GUI support"
msgstr "Bonobo-GUI-UnterstÃ¼tzung"


#: bonobo/bonobo-ui-sync-toolbar.c:556
msgid "B_oth"
msgstr "Bei_de"

#: bonobo/bonobo-ui-sync-toolbar.c:556
msgid "T_ext"
msgstr "T_ext"

#: bonobo/bonobo-ui-sync-toolbar.c:557
msgid "Hide t_ips"
msgstr "T_ipps verbergen"

#: bonobo/bonobo-ui-sync-toolbar.c:557
msgid "Show t_ips"
msgstr "T_ipps zeigen"

#: bonobo/bonobo-ui-sync-toolbar.c:558
# SUN CHANGED MESSAGE
msgid "_Hide toolbar"
msgstr "Symbolleiste _verbergen"


#: bonobo/bonobo-ui-sync-toolbar.c:558
msgid "Customi_ze"
msgstr "_Anpassen"

#: bonobo/bonobo-ui-sync-toolbar.c:559
# SUN CHANGED MESSAGE
msgid "Customize the toolbar"
msgstr "Symbolleiste anpassen"


#: bonobo/bonobo-ui-toolbar.c:1095
# SUN CHANGED MESSAGE
msgid "is floating"
msgstr "ist unverankert"


#: bonobo/bonobo-ui-toolbar.c:1096
# SUN CHANGED MESSAGE
msgid "whether the toolbar is floating"
msgstr "Ob die Symbolleiste unverankert ist"


#: bonobo/bonobo-ui-util.c:583
# SUN NEW TRANSLATION
msgid "_Contents"
msgstr "_Inhalt"


#: bonobo/bonobo-ui-util.c:584
#, fuzzy
# SUN CHANGED MESSAGE
msgid "View help for this application"
msgstr "Hilfe fÃ¼r diese Anwendung anzeigen lassen"


#: bonobo/bonobo-window.c:448
# SUN NEW TRANSLATION
msgid "Name of the window - used for configuration serialization."
msgstr "Name des Fensters, das fÃ¼r die serielle Konfiguration verwendet wird"


#: bonobo/bonobo-zoomable.c:326
msgid "Zoom level"
msgstr "Zoomstufe"

#: bonobo/bonobo-zoomable.c:327
# SUN CHANGED MESSAGE
msgid "The degree of enlargment"
msgstr "Der Grad der VergrÃ¶Ãerung"


#: bonobo/bonobo-zoomable.c:335
msgid "Minimum Zoom level"
msgstr "Minimale Zoomstufe"

# CHECK vielleicht nicht ganz perfekt
#: bonobo/bonobo-zoomable.c:336
# SUN CHANGED MESSAGE
msgid "The minimum degree of enlargment"
msgstr "Der minimale Grad der VergrÃ¶Ãerung"


#: bonobo/bonobo-zoomable.c:344
msgid "Maximum Zoom level"
msgstr "Maximale Zoomstufe"

#: bonobo/bonobo-zoomable.c:345
# SUN CHANGED MESSAGE
msgid "The maximum degree of enlargment"
msgstr "Der maximale Grad der VergrÃ¶Ãerung"


#: bonobo/bonobo-zoomable.c:353
msgid "Has a minimum Zoom level"
msgstr "Hat eine minimale Zoomstufe"

#: bonobo/bonobo-zoomable.c:354
# SUN CHANGED MESSAGE
msgid "Whether we have a valid minimum zoom level"
msgstr "Ob es eine gÃ¼ltige minimale Zoomstufe gibt"


#: bonobo/bonobo-zoomable.c:361
msgid "Has a maximum Zoom level"
msgstr "Hat eine maximale Zoomstufe"

#: bonobo/bonobo-zoomable.c:362
# SUN CHANGED MESSAGE
msgid "Whether we have a valid maximum zoom level"
msgstr "Ob es eine gÃ¼ltige maximale Zoomstufe gibt"


#: bonobo/bonobo-zoomable.c:369
msgid "Is continuous"
msgstr "Ist kontinuierlich"

#: bonobo/bonobo-zoomable.c:370
# SUN CHANGED MESSAGE
msgid "Whether we zoom continuously (as opposed to jumps)"
msgstr "Ob kontinuierlich gezoomt wird (im Gegensatz zu SprÃ¼ngen)"


#: samples/bonoboui/Bonobo_Sample_Hello.xml.h:1
# SUN CHANGED MESSAGE
msgid "About this application"
msgstr "Info Ã¼ber diese Anwendung"


#: samples/bonoboui/Bonobo_Sample_Hello.xml.h:2
# SUN CHANGED MESSAGE
msgid "Select"
msgstr "AuswÃ¤hlen"


#: samples/bonoboui/Bonobo_Sample_Hello.xml.h:3
msgid "_About..."
msgstr "_Info..."

#: samples/bonoboui/Bonobo_Sample_Hello.xml.h:4
msgid "_Edit"
msgstr "B_earbeiten"

#: samples/bonoboui/Bonobo_Sample_Hello.xml.h:5
msgid "_File"
msgstr "_Datei"

#: samples/bonoboui/Bonobo_Sample_Hello.xml.h:6
# SUN CHANGED MESSAGE
msgid "_Select"
msgstr "_AuswÃ¤hlen"


#: samples/bonoboui/hello.c:63
msgid "This does nothing; it is only a demonstration."
msgstr "Dies tut nichts; es ist nur eine Demonstration."

#: samples/bonoboui/hello.c:145
msgid "BonoboUI-Hello."
msgstr "BonoboUI-Hallo."

#: samples/bonoboui/hello.c:217
msgid "Gnome Hello"
msgstr "GNOME-Hallo"

#. Create Label and put it in the Button:
#: samples/bonoboui/hello.c:272
msgid "Hello, World!"
msgstr "Hallo, Welt!"

#: samples/bonoboui/hello.c:312
# SUN CHANGED MESSAGE
msgid "Cannot init libbonoboui code"
msgstr "Der libbonoboui-Code konnte nicht initialisiert werden."


#: samples/controls/bonobo-sample-controls.c:131
# SUN NEW TRANSLATION
msgid "Could not initialize Bonobo UI"
msgstr "Bonobo-UI konnte nicht initialisiert werden."

