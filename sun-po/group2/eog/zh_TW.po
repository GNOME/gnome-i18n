# traditional Chinese translation of eog.
# Copyright (C) 2000-2002 Free Software Foundation, Inc.
# Jing-Jong Shyue <shyue@sonoma.com.tw>,2000
# Abel Cheung <maddog@linux.org.hk>, 2001-2002
#
msgid ""
msgstr ""
"Project-Id-Version: eog 1.0.0\n"
"POT-Creation-Date: 2002-07-24 11:55+0100\n"
"PO-Revision-Date: 2002-07-24 11:58+0100\n"
"Last-Translator: Sun G11n <gnome_int_l10n@ireland.sun.com>\n"
"Language-Team: Sun G11n <gnome_int_l10n@ireland.sun.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8-bit\n"

#: collection/GNOME_EOG_Collection.server.in.h:1
msgid "EOG Image Collection"
msgstr "EOG 圖像集"

#: collection/GNOME_EOG_Collection.server.in.h:2
msgid "EOG Image Collection Viewer"
msgstr "EOG 圖像集瀏覽程式"

#: collection/GNOME_EOG_Collection.server.in.h:3
msgid "EOG Image collection view factory"
msgstr "EOG 圖像集瀏覽程式工廠"

#: eog.desktop.in.h:1
msgid "Eye of Gnome Image Viewer"
msgstr "Eye of Gnome 圖像瀏覽程式"

#: eog.desktop.in.h:2
msgid "View many different types of images"
msgstr "瀏覽不同種類的圖像"

# src/window.c:704
#: libeog/GNOME_EOG.server.in.in.h:1 viewer/eog-image-view.c:691
msgid "EOG Image"
msgstr "EOG 圖像"

#: libeog/GNOME_EOG.server.in.in.h:2
msgid "EOG Image Viewer"
msgstr "EOG 圖像瀏覽程式"

#: libeog/GNOME_EOG.server.in.in.h:3
msgid "EOG Image viewer factory"
msgstr "EOG 圖像瀏覽程式工廠"

#: libeog/GNOME_EOG.server.in.in.h:4
msgid "Embeddable EOG Image"
msgstr "可嵌入式 EOG 圖像"

# src/preferences-dialog.glade.h:7
#: libeog/image-view.c:1853
msgid "interpolation type"
msgstr "插值法方式"

# src/preferences-dialog.glade.h:7
#: libeog/image-view.c:1854
msgid "the type of interpolation to use"
msgstr "使用的插值法"

# src/preferences-dialog.glade.h:9
#: libeog/image-view.c:1860 libeog/image-view.c:1867
msgid "check type"
msgstr "方格類型"

#: libeog/image-view.c:1861
msgid "the type of chequering to use"
msgstr "繪畫方格的方式"

#: libeog/image-view.c:1868
msgid "the size of chequers to use"
msgstr "方格的大小"

# src/preferences-dialog.glade.h:26
#: libeog/image-view.c:1874
msgid "dither"
msgstr "擬色(dither)"

# src/preferences-dialog.glade.h:9
#: libeog/image-view.c:1875
msgid "dither type"
msgstr "擬色方式"

#: shell/eog-preferences.c:162
#, c-format
msgid ""
"Could not display help for the preferences dialog.\n"
"%s"
msgstr ""
"無法顯示有關偏好設定對話窗的說明文件。\n"
"%s"

# src/window.c:162
# src/window.c:573
# src/window.c:812
#: shell/eog-preferences.c:214
msgid "Eye of Gnome Preferences"
msgstr "Eye of Gnome 偏好設定"

#: shell/eog-shell-ui.xml.h:1
msgid "About this application"
msgstr "關於此應用程式"

#: shell/eog-shell-ui.xml.h:2
msgid "Cancel"
msgstr "取消"

# src/preferences.c:219
#: shell/eog-shell-ui.xml.h:3
msgid "Change preferences"
msgstr "更改偏好設定"

# src/tb-image.c:35
#: shell/eog-shell-ui.xml.h:4
msgid "Close"
msgstr "關閉"

# src/window.c:186
#: shell/eog-shell-ui.xml.h:5
msgid "Close window"
msgstr "關閉視窗"

#: shell/eog-shell-ui.xml.h:6
msgid "Contents"
msgstr "內容"

#: shell/eog-shell-ui.xml.h:7
msgid "Help On this application"
msgstr "關於此應用程式的說明"

# src/tb-image.c:33
#: shell/eog-shell-ui.xml.h:8 shell/main.c:205
msgid "Open"
msgstr "開啟"

# src/tb-image.c:33
# src/window.c:181
#: shell/eog-shell-ui.xml.h:9
msgid "Open a file"
msgstr "開啟檔案"

# src/preferences-dialog.glade.h:38
#: shell/eog-shell-ui.xml.h:10
msgid "Open a new window"
msgstr "開啟新視窗"

# src/preferences-dialog.glade.h:38
#: shell/eog-shell-ui.xml.h:11
msgid "Open in new window"
msgstr "在新視窗中開啟"

# src/preferences-dialog.glade.h:38
#: shell/eog-shell-ui.xml.h:12
msgid "Open in this window"
msgstr "在此視窗中開啟"

#: shell/eog-shell-ui.xml.h:13
msgid "Quit the program"
msgstr "離開此程式"

#: shell/eog-shell-ui.xml.h:14
msgid "_About"
msgstr "關於(_A)"

# src/tb-image.c:35
#: shell/eog-shell-ui.xml.h:15
msgid "_Close"
msgstr "關閉(_C)"

#: shell/eog-shell-ui.xml.h:16
msgid "_Edit"
msgstr "編輯(_E)"

#: shell/eog-shell-ui.xml.h:17
msgid "_File"
msgstr "檔案(_F)"

#: shell/eog-shell-ui.xml.h:18
msgid "_Help"
msgstr "說明(_H)"

# src/preferences-dialog.glade.h:32
#: shell/eog-shell-ui.xml.h:19
msgid "_New Window"
msgstr "新增視窗(_N)"

# src/window.c:181
#: shell/eog-shell-ui.xml.h:20
msgid "_Open..."
msgstr "開啟(_O)..."

# src/preferences.c:219
#: shell/eog-shell-ui.xml.h:21
msgid "_Preferences..."
msgstr "偏好設定(_P)..."

#: shell/eog-shell-ui.xml.h:22
msgid "_Quit"
msgstr "離開(_Q)"

# src/preferences-dialog.glade.h:49
#: shell/eog-shell-ui.xml.h:23
msgid "_View"
msgstr "瀏覽(_V)"

#. Translators should localize the following string
#. * which will give them credit in the About box.
#. * E.g. "Fulano de Tal <fulano@detal.com>"
#.
#: shell/eog-window.c:238
msgid "translator_credits-PLEASE_ADD_YOURSELF_HERE"
msgstr ""
"Abel Cheung <maddog@linux.org.hk>, 2001-2002\n"
"Jing-Jong Shyue <shyue@sonoma.com.tw>, 2000"

# src/window.c:162
# src/window.c:573
# src/window.c:812
#: shell/eog-window.c:243 shell/eog-window.c:632 shell/main.c:449
msgid "Eye of Gnome"
msgstr "Eye of Gnome"

# src/window.c:164
#: shell/eog-window.c:245
msgid "Copyright (C) 2000-2002 The Free Software Foundation"
msgstr "版權所有 (C) 2000-2002 The Free Software Foundation"

# src/window.c:166
#: shell/eog-window.c:246
msgid "The GNOME image viewing and cataloging program."
msgstr "GNOME 圖像瀏覽及分類程式。"

#: shell/eog-window.c:283
#, c-format
msgid ""
"Could not display help for Eye of Gnome.\n"
"%s"
msgstr ""
"無法顯示 Eye of Gnome 的說明文件。\n"
"%s"

# src/window.c:704
#: shell/eog-window.c:798
msgid "Open Image"
msgstr "開啟圖像"

#: shell/eog-window.c:1258
#, c-format
msgid "Unable to open %s."
msgstr "無法開啟 %s。"

#: shell/main.c:172
#, c-format
msgid ""
"You are about to open %i windows\n"
"simultanously. Do you want to open\n"
"them in a collection instead?"
msgstr ""
"將會同時開啟 %i 個視窗。\n"
"是否以圖像集方式開啟？"

# src/preferences-dialog.glade.h:32
#: shell/main.c:177
msgid "Single Windows"
msgstr "個別視窗"

#: shell/main.c:178
msgid "Collection"
msgstr "圖像集"

#: shell/main.c:202
#, c-format
msgid ""
"You are about to open %i windows\n"
"simultanously. Do you want to continue?"
msgstr ""
"將會同時開啟 %i 個\n"
"視窗。是否繼續？"

#: shell/main.c:285
#, c-format
msgid ""
"Could not access %s\n"
"Eye of Gnome will not be able to display this file."
msgstr ""
"無法存取 %s\n"
"Eye of Gnome 將不能顯示此檔案。"

#: shell/main.c:289
#, c-format
msgid ""
"The following files cannot be displayed because Eye of Gnome was not able to "
"access them:\n"
"%s"
msgstr ""
"因無法存取，Eye of Gnome 將不能顯示以下的檔案：\n"
"%s"

#: shell/main.c:461
msgid "Could not initialize GnomeVFS!\n"
msgstr "無法初始化 GnomeVFS！\n"

#: shell/main.c:464
msgid "Could not initialize Bonobo!\n"
msgstr "無法初始化 Bonobo！\n"

# src/util.c:47
#: shell/util.c:48
#, c-format
msgid "Could not open `%s'"
msgstr "無法開啟 `%s'"

#: viewer/eog-image-view-ctrl-ui.xml.h:1
msgid "1:1"
msgstr "1:1"

# src/tb-image.c:46
#: viewer/eog-image-view-ctrl-ui.xml.h:2
msgid "Fit"
msgstr "符合大小"

# src/tb-image.c:43
#: viewer/eog-image-view-ctrl-ui.xml.h:3 viewer/eog-print-setup.c:79
msgid "In"
msgstr "拉近"

# src/tb-image.c:44
#: viewer/eog-image-view-ctrl-ui.xml.h:4
msgid "Out"
msgstr "拉遠"

# src/window.c:229
#: viewer/eog-image-view-ctrl-ui.xml.h:5
msgid "Zoom _1:1"
msgstr "原來大小(_1)"

# src/window.c:221
#: viewer/eog-image-view-ctrl-ui.xml.h:6
msgid "Zoom _In"
msgstr "拉近(_I)"

# src/window.c:225
#: viewer/eog-image-view-ctrl-ui.xml.h:7
msgid "Zoom _Out"
msgstr "拉遠(_O)"

# src/window.c:225
#: viewer/eog-image-view-ctrl-ui.xml.h:8
msgid "Zoom to _Fit"
msgstr "符合視窗尺寸(_F)"

# src/preferences-dialog.glade.h:9
#: viewer/eog-image-view-ui.xml.h:1
msgid "Check _size"
msgstr "方格大小(_S)"

# src/preferences-dialog.glade.h:9
#: viewer/eog-image-view-ui.xml.h:2
msgid "Check _type"
msgstr "方格類型(_T)"

# src/preferences-dialog.glade.h:39
#: viewer/eog-image-view-ui.xml.h:3
msgid "Full Screen"
msgstr "全螢幕"

#: viewer/eog-image-view-ui.xml.h:4 viewer/preferences.c:63
msgid "Large"
msgstr "大"

#: viewer/eog-image-view-ui.xml.h:5 viewer/preferences.c:62
msgid "Medium"
msgstr "中"

# src/preferences-dialog.glade.h:21
#: viewer/eog-image-view-ui.xml.h:6
msgid "Nea_rest Neighbour"
msgstr "最近點(_R)"

# src/preferences-dialog.glade.h:27
#: viewer/eog-image-view-ui.xml.h:7
msgid "Normal (_pseudocolor)"
msgstr "正常[pseudocolor]擬色(_P)"

#: viewer/eog-image-view-ui.xml.h:8
msgid "Previews the image to be printed"
msgstr "預覽準備列印的圖像"

#: viewer/eog-image-view-ui.xml.h:9
msgid "Print Pre_view"
msgstr "列印預覽(_V)"

#: viewer/eog-image-view-ui.xml.h:10
msgid "Print S_etup"
msgstr "列印設定(_E)"

#: viewer/eog-image-view-ui.xml.h:11
msgid "Print image to the printer"
msgstr "在印表機列印圖像"

#: viewer/eog-image-view-ui.xml.h:12
msgid "Setup the page settings for your current printer"
msgstr "更改目前印表機的頁面設定"

#: viewer/eog-image-view-ui.xml.h:13 viewer/preferences.c:61
msgid "Small"
msgstr "小"

#: viewer/eog-image-view-ui.xml.h:14
msgid "_Bilinear"
msgstr "雙線性(_B)"

#: viewer/eog-image-view-ui.xml.h:15
msgid "_Black"
msgstr "黑色(_B)"

#: viewer/eog-image-view-ui.xml.h:16
msgid "_Dark"
msgstr "深色(_D)"

# src/preferences-dialog.glade.h:26
#: viewer/eog-image-view-ui.xml.h:17
msgid "_Dither"
msgstr "擬色(_D)"

#: viewer/eog-image-view-ui.xml.h:18
msgid "_Gray"
msgstr "灰色(_G)"

# src/preferences-dialog.glade.h:7
#: viewer/eog-image-view-ui.xml.h:19
msgid "_Hyperbolic"
msgstr "雙曲線(_H)"

# src/preferences-dialog.glade.h:7
#: viewer/eog-image-view-ui.xml.h:20
msgid "_Interpolation"
msgstr "插值法(_I)"

#: viewer/eog-image-view-ui.xml.h:21
msgid "_Light"
msgstr "淺色(_L)"

#: viewer/eog-image-view-ui.xml.h:22
msgid "_Maximum (high color)"
msgstr "最高品質[高彩]擬色(_M)"

#: viewer/eog-image-view-ui.xml.h:23
msgid "_Midtone"
msgstr "半調色(_M)"

#: viewer/eog-image-view-ui.xml.h:24
msgid "_None"
msgstr "無(_N)"

#: viewer/eog-image-view-ui.xml.h:25
msgid "_Print"
msgstr "列印(_P)"

#: viewer/eog-image-view-ui.xml.h:26
msgid "_Save As..."
msgstr "另存新檔(_S)..."

#: viewer/eog-image-view-ui.xml.h:27
msgid "_Tiles"
msgstr "區塊(_T)"

#: viewer/eog-image-view-ui.xml.h:28
msgid "_White"
msgstr "白色(_W)"

#: viewer/eog-image-view.c:300
#, c-format
msgid "Could not save image as '%s': %s."
msgstr "無法將圖像儲存為‘%s’：%s。"

#: viewer/eog-image-view.c:351
msgid "Save As"
msgstr "另存新檔"

# src/window.c:704
#: viewer/eog-image-view.c:926
msgid "Print Image"
msgstr "列印圖像"

#: viewer/eog-image-view.c:934
msgid "Pages"
msgstr "頁"

#: viewer/eog-image-view.c:1021
msgid "Print Preview"
msgstr "列印預覽"

# src/tb-image.c:33
# src/window.c:181
#: viewer/eog-image-view.c:1027
msgid "Printing of image failed"
msgstr "列印圖像失敗"

# src/preferences-dialog.glade.h:31
#: viewer/eog-image-view.c:1843
msgid "Display"
msgstr "顯示"

# src/preferences-dialog.glade.h:7
#: viewer/eog-image-view.c:2064
msgid "Interpolation"
msgstr "插值法(Interpolation)"

# src/preferences-dialog.glade.h:26
#: viewer/eog-image-view.c:2067
msgid "Dither"
msgstr "擬色(Dither)"

# src/preferences-dialog.glade.h:9
#: viewer/eog-image-view.c:2070
msgid "Check Type"
msgstr "方格類型"

# src/preferences-dialog.glade.h:9
#: viewer/eog-image-view.c:2073
msgid "Check Size"
msgstr "方格大小"

# src/preferences-dialog.glade.h:32
#: viewer/eog-image-view.c:2076
msgid "Image Width"
msgstr "圖像寬度"

#: viewer/eog-image-view.c:2079
msgid "Image Height"
msgstr "圖像高度"

#: viewer/eog-image-view.c:2082
msgid "Window Title"
msgstr "視窗標題"

#: viewer/eog-image-view.c:2085
msgid "Statusbar Text"
msgstr "狀態列文字"

#: viewer/eog-image.c:470
msgid "Couldn't initialize GnomeVFS!\n"
msgstr "無法初始化 GnomeVFS！\n"

#: viewer/eog-print-setup.c:76
msgid "pts"
msgstr "點"

#: viewer/eog-print-setup.c:76
msgid "points"
msgstr "點"

#: viewer/eog-print-setup.c:77
msgid "mm"
msgstr "mm"

#: viewer/eog-print-setup.c:77
msgid "millimeter"
msgstr "毫米"

#: viewer/eog-print-setup.c:78
msgid "cm"
msgstr "cm"

#: viewer/eog-print-setup.c:78
msgid "centimeter"
msgstr "厘米"

#: viewer/eog-print-setup.c:79
msgid "inch"
msgstr "英吋"

#: viewer/eog-print-setup.c:469
msgid "Print Setup"
msgstr "列印設定"

#: viewer/eog-print-setup.c:476
msgid "Print"
msgstr "列印"

#: viewer/eog-print-setup.c:478
msgid "Print preview"
msgstr "列印預覽"

#: viewer/eog-print-setup.c:547
msgid "Units: "
msgstr "單位："

#. First page
#: viewer/eog-print-setup.c:557 viewer/eog-print-setup.c:565
msgid "Paper"
msgstr "紙張"

#: viewer/eog-print-setup.c:569
msgid "Paper size:"
msgstr "紙張大小："

#. Orientation
#: viewer/eog-print-setup.c:586
msgid "Orientation"
msgstr "方向"

#: viewer/eog-print-setup.c:593
msgid "Portrait"
msgstr "直向"

#: viewer/eog-print-setup.c:600
msgid "Landscape"
msgstr "橫向"

#. Margins
#: viewer/eog-print-setup.c:609
msgid "Margins"
msgstr "邊緣"

#. Top margin
#: viewer/eog-print-setup.c:617
msgid "Top:"
msgstr "頂："

#. Left margin
#: viewer/eog-print-setup.c:635
msgid "Left:"
msgstr "左："

#. Right margin
#: viewer/eog-print-setup.c:653
msgid "Right:"
msgstr "右："

#. Bottom margin
#: viewer/eog-print-setup.c:671
msgid "Bottom:"
msgstr "底："

# src/window.c:704
#. Second page
#: viewer/eog-print-setup.c:689
msgid "Image"
msgstr "圖像"

#. Scale
#: viewer/eog-print-setup.c:697
msgid "Scale"
msgstr "縮放"

#: viewer/eog-print-setup.c:701
msgid "Adjust to:"
msgstr "調校至："

#: viewer/eog-print-setup.c:706
msgid "Fit to page"
msgstr "符合頁面"

#: viewer/eog-print-setup.c:725
#, no-c-format
msgid "% of original size"
msgstr "原來大小的百分比"

#. Center on page
#: viewer/eog-print-setup.c:731
msgid "Center"
msgstr "中央"

#: viewer/eog-print-setup.c:735
msgid "Horizontally"
msgstr "水平"

#: viewer/eog-print-setup.c:742
msgid "Vertically"
msgstr "垂直"

#. Overlap
#: viewer/eog-print-setup.c:751
msgid "Overlapping"
msgstr "重疊"

#: viewer/eog-print-setup.c:755
msgid "Overlap horizontally by "
msgstr "橫向重疊的尺寸是"

#: viewer/eog-print-setup.c:760
msgid "Overlap vertically by "
msgstr "直向重疊的尺寸是"

#. Third page
#: viewer/eog-print-setup.c:797
msgid "Printing"
msgstr "列印"

#. Helpers
#: viewer/eog-print-setup.c:805
msgid "Helpers"
msgstr "輔助線"

#: viewer/eog-print-setup.c:806
msgid "Print cutting help"
msgstr "列印切割輔助線"

#: viewer/eog-print-setup.c:812
msgid "Print overlap help"
msgstr "列印重疊輔助線"

#. Page order
#: viewer/eog-print-setup.c:821
msgid "Page order"
msgstr "列印頁次序"

#. Down right
#: viewer/eog-print-setup.c:827
msgid "Down, then right"
msgstr "向下，然後向右"

#. Right down
#: viewer/eog-print-setup.c:844
msgid "Right, then down"
msgstr "向右，然後向下"

# src/preferences-dialog.glade.h:21
#: viewer/preferences.c:33
msgid "Nearest Neighbour Interpolation"
msgstr "最近點插值法"

# src/preferences-dialog.glade.h:7
#: viewer/preferences.c:35
msgid "Tiles Interpolation"
msgstr "區塊插值法"

# src/preferences-dialog.glade.h:7
#: viewer/preferences.c:37
msgid "Bilinear Interpolation"
msgstr "雙線性插值法"

# src/preferences-dialog.glade.h:7
#: viewer/preferences.c:39
msgid "Hyperbolic Interpolation"
msgstr "雙曲線插值法"

#: viewer/preferences.c:44
msgid "No dithering"
msgstr "不使用擬色"

# src/preferences-dialog.glade.h:27
#: viewer/preferences.c:45
msgid "Normal (pseudocolor) dithering"
msgstr "正常（pseudocolor）擬色"

#: viewer/preferences.c:46
msgid "Maximum (high color) dithering"
msgstr "最高品質（高彩）擬色"

#: viewer/preferences.c:51
msgid "Dark"
msgstr "深色"

#: viewer/preferences.c:52
msgid "Midtone"
msgstr "半調色"

#: viewer/preferences.c:53
msgid "Light"
msgstr "淺色"

#: viewer/preferences.c:54
msgid "Black"
msgstr "黑色"

#: viewer/preferences.c:55
msgid "Gray"
msgstr "灰色"

#: viewer/preferences.c:56
msgid "White"
msgstr "白色"

# src/preferences-dialog.glade.h:7
#: viewer/preferences.c:175
msgid "_Interpolation:"
msgstr "插值法(_I)："

# src/preferences-dialog.glade.h:26
#: viewer/preferences.c:187
msgid "_Dither:"
msgstr "擬色(_D)："

# src/preferences-dialog.glade.h:9
#: viewer/preferences.c:199
msgid "Check _type:"
msgstr "方格類型(_T)："

# src/preferences-dialog.glade.h:9
#: viewer/preferences.c:211
msgid "Check _size:"
msgstr "方格大小(_S)："

#~ msgid "scroll"
#~ msgstr "捲動"

#~ msgid "scroll type"
#~ msgstr "捲動方式"

#~ msgid "enable full screen zoom"
#~ msgstr "可使用全螢幕模式"

#~ msgid "whether we should allow full screen zoom"
#~ msgstr "是否允許使用全螢幕模式"

#~ msgid "Translator Credits"
#~ msgstr "Abel Cheung <maddog@linux.org.hk>"

#~ msgid "Control doesn't support window_title property."
#~ msgstr "此控制元件不支援 window_title 屬性。"

#~ msgid "Control doesn't support status_text property."
#~ msgstr "此控制元件不支援 status_text 屬性。"

#~ msgid "Control doesn't have properties"
#~ msgstr "此控制元件沒有任何屬性。"

#~ msgid "Couldn't retrieve mime type for file."
#~ msgstr "無法取得此檔案的 mime 類型。"

#~ msgid "Error while obtaining file informations."
#~ msgstr "讀入檔案資訊時發生錯誤。"

#~ msgid "_Settings"
#~ msgstr "設定(_S)"

# src/preferences-dialog.glade.h:9
#~ msgid "Check size large"
#~ msgstr "方格大小：大"

# src/preferences-dialog.glade.h:9
#~ msgid "Check size medium"
#~ msgstr "方格大小：中"

# src/preferences-dialog.glade.h:9
#~ msgid "Check size small"
#~ msgstr "方格大小：小"

# src/preferences-dialog.glade.h:9
#~ msgid "Check type _black"
#~ msgstr "方格類型：黑色(_B)"

# src/preferences-dialog.glade.h:9
#~ msgid "Check type _dark"
#~ msgstr "方格類型：深色(_D)"

# src/preferences-dialog.glade.h:9
#~ msgid "Check type _gray"
#~ msgstr "方格類型：灰色(_G)"

# src/preferences-dialog.glade.h:9
#~ msgid "Check type _light"
#~ msgstr "方格類型：淺色(_L)"

# src/preferences-dialog.glade.h:9
#~ msgid "Check type _midtone"
#~ msgstr "方格類型：半調色(_M)"

# src/preferences-dialog.glade.h:9
#~ msgid "Check type _white"
#~ msgstr "方格類型：白色(_W)"

#~ msgid "Save _As"
#~ msgstr "另存新檔(_A)"

# src/preferences-dialog.glade.h:7
#~ msgid "_Bilinear Interpolation"
#~ msgstr "雙線性插值法(_B)"

#~ msgid "_No dithering"
#~ msgstr "不使用擬色(_N)"

# src/preferences-dialog.glade.h:7
#~ msgid "_Tiles Interpolation"
#~ msgstr "區塊插值法(_T)"

#~ msgid "Zoom to _Default"
#~ msgstr "縮放至原來大小(_D)"

# src/preferences-dialog.glade.h:7
#~ msgid "Interpolation:"
#~ msgstr "插值法(Interpolation)："

# src/preferences-dialog.glade.h:26
#~ msgid "Dither:"
#~ msgstr "擬色(Dither)："

# src/preferences-dialog.glade.h:9
#~ msgid "Check Type:"
#~ msgstr "方格類型："

# src/preferences-dialog.glade.h:9
#~ msgid "Check Size:"
#~ msgstr "方格大小："

# src/preferences-dialog.glade.h:32
#~ msgid "Image Viewer"
#~ msgstr "圖像瀏覽程式"

#~ msgid "Vertical"
#~ msgstr "垂直"

#~ msgid "Horizontal"
#~ msgstr "水平"

#~ msgid "Rectangle"
#~ msgstr "長方形"

#~ msgid "Layout Mode:"
#~ msgstr "配置模式："

#~ msgid "Image Collection Background Color"
#~ msgstr "圖像集背景顏色"

#~ msgid "Background Color:"
#~ msgstr "背景顏色："

#~ msgid "Send"
#~ msgstr "送出"

#~ msgid "Collection View"
#~ msgstr "圖像集檢視模式"

#~ msgid "Status Text"
#~ msgstr "狀態文字"

# src/preferences.c:219
#~ msgid "Preferences"
#~ msgstr "偏好設定"

# src/tb-image.c:33
# src/window.c:181
#~ msgid "Open an image file"
#~ msgstr "開啟圖像檔"

# src/tb-image.c:35
# src/window.c:186
#~ msgid "Close the current window"
#~ msgstr "關閉目前視窗"

# src/window.c:704
#~ msgid "_Open Image..."
#~ msgstr "開啟圖像(_O)..."

# src/window.c:186
#~ msgid "_Close This Window"
#~ msgstr "關閉此視窗(_C)"

# src/window.c:221
#~ msgid "Increase zoom factor by 5%%"
#~ msgstr "增加 5%% 縮放倍率"

# src/window.c:225
#~ msgid "Decrease zoom factor by 5%%"
#~ msgstr "減少 5%% 縮放倍率"

# src/window.c:229
#~ msgid "Display the image at 1:1 scale"
#~ msgstr "將圖像以原來大小顯示"

# src/window.c:233
#~ msgid "_Zoom factor"
#~ msgstr "縮放倍率(_Z)"

# src/window.c:235
#~ msgid "_Fit to Window"
#~ msgstr "符合視窗大小(_F)"

# src/window.c:235
#~ msgid "Zoom the image to fit in the window"
#~ msgstr "將圖像縮放成符合視窗尺寸"

# src/preferences-dialog.glade.h:39
#~ msgid "Full _Screen"
#~ msgstr "全螢幕(_S)"

# src/window.c:239
#~ msgid "Use the whole screen for display"
#~ msgstr "使用整個畫面顯示"

# src/preferences-dialog.glade.h:25
#~ msgid "Two-pass scrolling"
#~ msgstr "二次捲動"

# src/preferences-dialog.glade.h:37
#~ msgid "Pick window size and zoom factor automatically"
#~ msgstr "自動設定視窗大小與縮放倍率"

# src/preferences-dialog.glade.h:44
#~ msgid "Use 1:1 zoom factor"
#~ msgstr "使用 1:1 縮放倍率"

# src/preferences-dialog.glade.h:45
#~ msgid "Use same zoom factor as image window"
#~ msgstr "使用與圖像視窗中相同的縮放倍率"

# src/preferences-dialog.glade.h:46
#~ msgid "Fit all images to screen"
#~ msgstr "圖像大小符合螢幕"

# src/preferences-dialog.glade.h:47
#~ msgid "Fit standard-sized images to screen"
#~ msgstr "將標準大小圖像符合螢幕"

# src/preferences-dialog.glade.h:48
#~ msgid "Put a bevel around the edge of the screen"
#~ msgstr "在畫面邊緣繪製斜面"
