# Italian localisation for GHEX
# Copyright (C) 2000 Free Software Foundation, Inc.
# FIRST AUTHOR: UNKNOWN (maybe Christopher R. Gabriel
# <cgabriel@pluto.linux.it>
#
msgid ""
msgstr ""
"Project-Id-Version: ghex 1.0.5\n"
"POT-Creation-Date: 2002-05-30 20:44+0100\n"
"PO-Revision-Date: 2002-05-30 04:40\n"
"Last-Translator: Sun G11n <gnome_int_l10n@ireland.sun.com>\n"
"Language-Team: Sun G11n <gnome_int_l10n@ireland.sun.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ghex.desktop.in.h:1
#, fuzzy
msgid "GHex"
msgstr "Esadecimale"

#: ghex.desktop.in.h:2
#, fuzzy
msgid "Gnome Hexadecimal Editor"
msgstr "Esadecimale"

#: src/bonobo-mdi.c:494 src/bonobo-mdi.c:570 src/bonobo-mdi.c:1886
#, c-format
msgid "Activate %s"
msgstr "Attiva %s"

#: src/chartable.c:115
msgid "ASCII"
msgstr "ASCII"

#: src/chartable.c:115
msgid "Hex"
msgstr "Esadecimale"

#: src/chartable.c:115 src/preferences.c:214
msgid "Decimal"
msgstr "Decimale"

#: src/chartable.c:116
msgid "Octal"
msgstr "Ottale"

#: src/chartable.c:116
msgid "Binary"
msgstr "Binario"

#: src/chartable.c:128
#, fuzzy
msgid "Character table"
msgstr "Tabella caratteri..."

#: src/converter.c:190
#, fuzzy
msgid "Base Converter"
msgstr "GHex: Convertitore"

#. entries
#: src/converter.c:209
#, fuzzy
msgid "_Binary:"
msgstr "_Binario"

#: src/converter.c:211
#, fuzzy
msgid "_Octal:"
msgstr "_Ottale"

#: src/converter.c:213
#, fuzzy
msgid "_Decimal:"
msgstr "_Decimale"

#: src/converter.c:215
#, fuzzy
msgid "_Hex:"
msgstr "_Esadecimale"

#: src/converter.c:217
#, fuzzy
msgid "_ASCII:"
msgstr "_ASCII"

#. get cursor button
#: src/converter.c:221
msgid "_Get cursor value"
msgstr "_Ottieni valore al cursore"

#: src/converter.c:231
#, fuzzy
msgid "Get cursor value"
msgstr "_Ottieni valore al cursore"

#: src/converter.c:231
msgid "Gets the value at cursor in binary, octal, decimal, hex and ASCII"
msgstr ""

#: src/converter.c:358
msgid "ERROR"
msgstr "ERRORE"

#: src/findreplace.c:60 src/main.c:118
#, c-format
msgid "GHex (%s): Find Data"
msgstr "GHex: (%s): Trova dati"

#: src/findreplace.c:69
#, c-format
msgid "Search for %s"
msgstr "Cerca %s"

#: src/findreplace.c:86 src/findreplace.c:119
msgid "Find Next"
msgstr "Trova il successivo"

#: src/findreplace.c:94
msgid "Find Previous"
msgstr "Trova il precedente"

#: src/findreplace.c:118 src/findreplace.c:210
#, fuzzy
msgid "Find Data"
msgstr "Trova il successivo"

#: src/findreplace.c:118 src/findreplace.c:210
msgid "Enter the hex data or ASCII data to search for"
msgstr ""

#: src/findreplace.c:119 src/findreplace.c:212
msgid "Finds the next occurrence of the search string"
msgstr ""

#: src/findreplace.c:120
#, fuzzy
msgid "Find previous"
msgstr "Trova il precedente"

#: src/findreplace.c:120
msgid "Finds the previous occurrence of the search string "
msgstr ""

#: src/findreplace.c:121 src/findreplace.c:215 src/findreplace.c:263
msgid "Cancel"
msgstr ""

#: src/findreplace.c:121
msgid "Closes find data window"
msgstr ""

#: src/findreplace.c:140 src/main.c:120
#, c-format
msgid "GHex (%s): Find & Replace Data"
msgstr "GHex (%s): Cerca & sostituisci dati"

#: src/findreplace.c:155
#, c-format
msgid "Replace %s"
msgstr "Sostituisci %s"

#: src/findreplace.c:172 src/findreplace.c:212
msgid "Find next"
msgstr "Trova il successivo"

#: src/findreplace.c:180 src/findreplace.c:213 src/ghex-ui.xml.h:43
msgid "Replace"
msgstr "Sostituisci"

#: src/findreplace.c:188 src/findreplace.c:214
msgid "Replace All"
msgstr "Sostituisci tutti"

#: src/findreplace.c:211
#, fuzzy
msgid "Replace Data"
msgstr "Sostituisci"

#: src/findreplace.c:211
msgid "Enter the hex data or ASCII data to replace with"
msgstr ""

#: src/findreplace.c:213
msgid "Replaces the search string with the replace string"
msgstr ""

#: src/findreplace.c:214
msgid "Replaces all occurrences of the search string with the replace string"
msgstr ""

#: src/findreplace.c:215
msgid "Closes find and replace data window"
msgstr ""

#: src/findreplace.c:231 src/main.c:122
#, c-format
msgid "GHex (%s): Jump To Byte"
msgstr "GHex (%s): Salta a byte"

#: src/findreplace.c:261
msgid "Jump to byte"
msgstr ""

#: src/findreplace.c:261
msgid "Enter the byte to jump to"
msgstr ""

#: src/findreplace.c:262
msgid "OK"
msgstr ""

#: src/findreplace.c:262
msgid "Jumps to the specified byte"
msgstr ""

#: src/findreplace.c:263
msgid "Closes jump to byte window"
msgstr ""

#: src/findreplace.c:328 src/findreplace.c:354 src/findreplace.c:407
msgid "There is no active buffer to search!"
msgstr "Non c'è nessun buffer attivo in cui cercare!"

#: src/findreplace.c:336 src/findreplace.c:362 src/findreplace.c:415
msgid "There seems to be no string to search for!"
msgstr "Non c'è nessuna stringa da cercare!"

#: src/findreplace.c:343 src/findreplace.c:423
msgid "End Of File reached"
msgstr "Fine del file raggiunta"

#: src/findreplace.c:370
msgid "Beginning Of File reached"
msgstr "Inizio del file raggiunto"

#: src/findreplace.c:380
msgid "There is no active buffer to move the cursor in!"
msgstr "Non c'è nessun buffer attivo in cui muovere il cursore!"

#: src/findreplace.c:385
msgid "No offset has been specified!"
msgstr "Nessun offset è stato specificato!"

#: src/findreplace.c:392
msgid "Can not position cursor beyond the End Of File!"
msgstr "Impossibile posizionare il cursore oltre la file del file!"

#: src/findreplace.c:397
msgid "The offset must be a positive integer value!"
msgstr "L'offset deve essere un valore intero positivo!"

#: src/findreplace.c:435 src/findreplace.c:473
msgid "There is no active buffer to replace data in!"
msgstr "Non c'è nessun buffer attivo in cui sostituire i dati!"

#: src/findreplace.c:446 src/findreplace.c:484
msgid "Strange find or replace string!"
msgstr "Strana stringa di ricerca o sostituzione!"

#: src/findreplace.c:461
msgid "End Of File reached!"
msgstr "Fine del file raggiunta!"

#: src/findreplace.c:500
#, c-format
msgid "Replaced %d occurencies."
msgstr "Sostituite %d occorrenze."

#: src/ghex-mdi.c:490
#, c-format
msgid ""
"Do you want to save the changes you made to the document ``%s''? \n"
"\n"
"Your changes will be lost if you don't save them."
msgstr ""
"Desiderate salvare le modifiche apportate al file ``%s''? \n"
"\n"
"Le modifiche saranno perse se non verrano salvate."

#: src/ghex-mdi.c:495
msgid "_Don't save"
msgstr "_Non salvare"

#: src/ghex-mdi.c:573
msgid "(modified)"
msgstr "(modificato)"

#: src/ghex-mdi.c:579
msgid "(readonly)"
msgstr "(sola lettura)"

#: src/ghex-mdi.c:887 src/main.c:239
#, c-format
msgid "Offset: %s"
msgstr "Offset: %s"

#: src/ghex-ui.xml.h:1
msgid "About this application"
msgstr "Informazioni su questa applicazione"

#: src/ghex-ui.xml.h:2
msgid "About..."
msgstr "Informazioni..."

#: src/ghex-ui.xml.h:3
msgid "Add View"
msgstr "Aggiungi vista"

#: src/ghex-ui.xml.h:4
msgid "Add a new view to the buffer"
msgstr "Aggiungi una nuova vista del buffer"

#: src/ghex-ui.xml.h:5 src/ui.c:119
msgid "Bytes"
msgstr "Byte"

#: src/ghex-ui.xml.h:6
msgid "Character Table..."
msgstr "Tabella caratteri..."

#: src/ghex-ui.xml.h:7 src/ui.c:85
msgid "Character _Table..."
msgstr "_Tabella caratteri..."

#: src/ghex-ui.xml.h:8
msgid "Close"
msgstr "Chiudi"

#: src/ghex-ui.xml.h:9
msgid "Close the current file"
msgstr "Chiude il file corrente"

#: src/ghex-ui.xml.h:10
msgid "Configure the application"
msgstr "Configura l'applicazione"

#: src/ghex-ui.xml.h:11
#, fuzzy
msgid "Contents"
msgstr "Caratteri"

#: src/ghex-ui.xml.h:12
msgid "Converter..."
msgstr "Con_vertitore"

#: src/ghex-ui.xml.h:13
msgid "E_xit"
msgstr "_Esci"

#: src/ghex-ui.xml.h:14
msgid "Exit"
msgstr "Esci"

#: src/ghex-ui.xml.h:15
msgid "Exit the program"
msgstr "Termina l'applicazione"

#: src/ghex-ui.xml.h:16 src/ui.c:66
msgid "Export data to HTML source"
msgstr "Esporti i dati in formato HTML"

# SUN CHANGED MESSAGE
#: src/ghex-ui.xml.h:17
msgid "Export to HTML..."
msgstr "Esporta in _HTML"

#: src/ghex-ui.xml.h:18
msgid "Fi_les"
msgstr "Fi_le"

#: src/ghex-ui.xml.h:19
msgid "Find"
msgstr "Trova"

#: src/ghex-ui.xml.h:20
msgid "Goto Byte"
msgstr "Vai al byte"

#: src/ghex-ui.xml.h:21
msgid "Group data by 16 bits"
msgstr "Raggruppa i dati 16 bit per volta"

#: src/ghex-ui.xml.h:22
msgid "Group data by 32 bits"
msgstr "Raggruppa i dati 32 bit per volta"

#: src/ghex-ui.xml.h:23
msgid "Group data by 8 bits"
msgstr "Raggruppa i dati 8 bit per volta"

#: src/ghex-ui.xml.h:24
msgid "Help Chat"
msgstr "Chat di supporto"

#: src/ghex-ui.xml.h:25
#, fuzzy
msgid "Help on this application"
msgstr "Informazioni su questa applicazione"

#: src/ghex-ui.xml.h:26
msgid "Insert Mode"
msgstr "Modo _inserimento"

#: src/ghex-ui.xml.h:27
msgid "Insert/overwrite data"
msgstr "Inserisce/sovrascrive dati"

#: src/ghex-ui.xml.h:28
msgid "Jump to a certain position"
msgstr "Salta a una posizione specificata"

#: src/ghex-ui.xml.h:29 src/ui.c:121
msgid "Longwords"
msgstr "Longword"

#: src/ghex-ui.xml.h:30
msgid "Open"
msgstr "Apri"

#: src/ghex-ui.xml.h:31
msgid "Open a file"
msgstr "Apre un file"

#: src/ghex-ui.xml.h:32 src/ui.c:84
msgid "Open base conversion dialog"
msgstr "Apri la finestra di conversione della base"

#: src/ghex-ui.xml.h:33
msgid "Preferences"
msgstr "Preferenze"

#: src/ghex-ui.xml.h:34 src/ui.c:70
msgid "Preview printed data"
msgstr "Anteprima dei dati stampati"

#: src/ghex-ui.xml.h:35
msgid "Print"
msgstr "Stampa"

#: src/ghex-ui.xml.h:36
#, fuzzy
msgid "Print Previe_w..."
msgstr "Anteprima di stampa..."

#: src/ghex-ui.xml.h:37
msgid "Print Preview..."
msgstr "Anteprima di stampa..."

#: src/ghex-ui.xml.h:38
msgid "Print the current file"
msgstr "Stampa il file corrente"

#: src/ghex-ui.xml.h:39
msgid "Redo"
msgstr "Ripeti"

#: src/ghex-ui.xml.h:40
msgid "Redo the undone action"
msgstr "Ripeti l'operazione annullata"

#: src/ghex-ui.xml.h:41
msgid "Remove View"
msgstr "Rimuovi vista"

#: src/ghex-ui.xml.h:42
msgid "Remove the current view of the buffer"
msgstr "Rimuovi la vista corrente del buffer"

#: src/ghex-ui.xml.h:44
msgid "Replace a string"
msgstr "Sostituisci una stringa"

#: src/ghex-ui.xml.h:45
msgid "Revert"
msgstr "Ripristina"

#: src/ghex-ui.xml.h:46
msgid "Revert to a saved version of the file"
msgstr "Ripristina ad una versione salvata del file"

#: src/ghex-ui.xml.h:47
msgid "Save"
msgstr "Salva"

#: src/ghex-ui.xml.h:48
msgid "Save As"
msgstr "Salva con nome..."

#: src/ghex-ui.xml.h:49
#, fuzzy
msgid "Save As _HTML..."
msgstr "Salva con _nome..."

#: src/ghex-ui.xml.h:50
msgid "Save _As..."
msgstr "Salva con _nome..."

#: src/ghex-ui.xml.h:51
msgid "Save the current file"
msgstr "Salva il file corrente"

#: src/ghex-ui.xml.h:52
msgid "Save the current file with a different name"
msgstr "Salva il file corrente con un nuovo nome"

#: src/ghex-ui.xml.h:53
msgid "Search for a string"
msgstr "Cerca una stringa"

#: src/ghex-ui.xml.h:54 src/ui.c:86
msgid "Show the character table"
msgstr "Mostra la tabella dei caratteri"

#: src/ghex-ui.xml.h:55
msgid "Undo"
msgstr "Annulla"

#: src/ghex-ui.xml.h:56
msgid "Undo the last action"
msgstr "Annulla l'ultima operazione"

#: src/ghex-ui.xml.h:57 src/ui.c:120
msgid "Words"
msgstr "Word"

#: src/ghex-ui.xml.h:58
msgid "_About..."
msgstr "_Informazioni..."

#: src/ghex-ui.xml.h:59
msgid "_Add View"
msgstr "_Aggiungi una vista"

#: src/ghex-ui.xml.h:60
#, fuzzy
msgid "_Base Converter"
msgstr "GHex: Convertitore"

#: src/ghex-ui.xml.h:61
msgid "_Bytes"
msgstr "_Byte"

#: src/ghex-ui.xml.h:62
msgid "_Close"
msgstr "_Chiudi"

#: src/ghex-ui.xml.h:63
msgid "_Contents"
msgstr ""

#: src/ghex-ui.xml.h:64
msgid "_Edit"
msgstr "_Modifica"

#: src/ghex-ui.xml.h:65
msgid "_File"
msgstr "_File"

#: src/ghex-ui.xml.h:66
msgid "_Find"
msgstr "_Trova"

#: src/ghex-ui.xml.h:67
msgid "_Goto Byte..."
msgstr "_Vai a byte..."

#: src/ghex-ui.xml.h:68
msgid "_Group Data As"
msgstr "_Raggruppa dati come"

#: src/ghex-ui.xml.h:69
msgid "_Help"
msgstr "_Aiuto"

#: src/ghex-ui.xml.h:70
msgid "_Insert Mode"
msgstr "Modo _inserimento"

#: src/ghex-ui.xml.h:71
msgid "_Longwords"
msgstr "_Longword"

#: src/ghex-ui.xml.h:72
msgid "_Open..."
msgstr "_Apri..."

#: src/ghex-ui.xml.h:73
msgid "_Preferences"
msgstr "_Preferenze..."

#: src/ghex-ui.xml.h:74
msgid "_Print"
msgstr "_Stampa"

#: src/ghex-ui.xml.h:75
msgid "_Redo"
msgstr "_Ripeti"

#: src/ghex-ui.xml.h:76
msgid "_Remove View"
msgstr "_Rimuovi vista"

#: src/ghex-ui.xml.h:77
msgid "_Replace"
msgstr "_Sostituisci"

#: src/ghex-ui.xml.h:78
msgid "_Revert"
msgstr "_Ripristina"

#: src/ghex-ui.xml.h:79
msgid "_Save"
msgstr "_Salva"

#: src/ghex-ui.xml.h:80 src/ui.c:103
msgid "_Tools"
msgstr "_Strumenti"

#: src/ghex-ui.xml.h:81
msgid "_Undo"
msgstr "_Annulla"

#: src/ghex-ui.xml.h:82
msgid "_View"
msgstr "_Vista"

#: src/ghex-ui.xml.h:83
msgid "_Words"
msgstr "_Words"

#: src/hex-document.c:749
msgid "Page"
msgstr "Pagina"

#: src/hex-document.c:755 src/hex-document.c:856
msgid "Hex dump generated by"
msgstr "Dump esadecimale generato da"

#: src/hex-document.c:779
msgid "Previous page"
msgstr "Pagina precedente"

#: src/hex-document.c:794
msgid "Next page"
msgstr "Pagina successiva"

#: src/main.c:56
#, c-format
msgid ""
"File %s has changed since last save.\n"
"Do you want to save changes?"
msgstr ""
"Il file %s è stato modificato dopo l'ultimo salvataggio.\n"
"Salvare le modifiche?"

#: src/main.c:67
#, fuzzy
msgid "Do_n't save"
msgstr "_Non salvare"

#: src/main.c:352
msgid "The gnome binary editor"
msgstr "L'editor Gnome per file binari"

#: src/preferences.c:57
msgid "Default"
msgstr "Default"

#: src/preferences.c:58
msgid "Notebook"
msgstr "Notebook"

#: src/preferences.c:59
msgid "Toplevel"
msgstr "Toplevel"

#: src/preferences.c:60
msgid "Modal"
msgstr "Modale"

#: src/preferences.c:92
msgid "Select font"
msgstr "Seleziona carattere:"

#: src/preferences.c:148
#, fuzzy
msgid "GHex Preferences"
msgstr "Preferenze"

#: src/preferences.c:176
#, fuzzy
msgid "_Maximum number of undo levels:"
msgstr "Massimo numero di livelli di annullamento modifiche"

#: src/preferences.c:196
#, fuzzy
msgid "Undo levels"
msgstr "Toplevel"

#: src/preferences.c:196
#, fuzzy
msgid "Select maximum number of undo levels"
msgstr "Massimo numero di livelli di annullamento modifiche"

#: src/preferences.c:200
#, fuzzy
msgid "Show cursor offset in statusbar as:"
msgstr "Mostra la posizione del cursore nella barra di stato come"

#: src/preferences.c:219
msgid "Hexadecimal"
msgstr "Esadecimale"

#: src/preferences.c:225
msgid "Custom"
msgstr "Personalizzato"

#: src/preferences.c:236
msgid "format_entry"
msgstr ""

#: src/preferences.c:236
msgid "Enter the cursor offset format"
msgstr ""

#: src/preferences.c:237
msgid "format_optionmenu"
msgstr ""

#: src/preferences.c:237
msgid "Select the cursor offset format"
msgstr ""

#. show offsets check button
#: src/preferences.c:247
msgid "Show offsets column"
msgstr "Mostra la posizione della colonna"

#: src/preferences.c:252
msgid "Editing"
msgstr "Editing"

#. display font
#: src/preferences.c:261
msgid "Font"
msgstr "Font"

#: src/preferences.c:283
msgid "Browse..."
msgstr "Sfoglia..."

#. default group type
#: src/preferences.c:312
msgid "Default Group Type"
msgstr "Tipo di gruppo di default"

#: src/preferences.c:328
msgid "Display"
msgstr "Visualizzazione"

#. mdi modes
#: src/preferences.c:337
msgid "MDI Mode"
msgstr "Modo MDI"

#: src/preferences.c:354
msgid "MDI"
msgstr "MDI"

#. paper selection
#: src/preferences.c:363
msgid "Paper size"
msgstr "Caratteri"

#. data & header font selection
#: src/preferences.c:379
msgid "Fonts"
msgstr "Caratteri"

#: src/preferences.c:385
#, fuzzy
msgid "_Data font:"
msgstr "Carattere dati"

#: src/preferences.c:403
msgid "Data font"
msgstr "Carattere dati"

#: src/preferences.c:403
#, fuzzy
msgid "Select the data font"
msgstr "Seleziona carattere:"

# SUN CHANGED MESSAGE
#: src/preferences.c:424
#, fuzzy
msgid "Header fo_nt:"
msgstr "Carattere intestazione"

# SUN CHANGED MESSAGE
#: src/preferences.c:444
msgid "Header font"
msgstr "Carattere intestazione"

#: src/preferences.c:444
#, fuzzy
msgid "Select the header font"
msgstr "Seleziona carattere:"

#: src/preferences.c:476
#, fuzzy
msgid "_Print shaded box over:"
msgstr "Stampa bordo a rilievo"

#: src/preferences.c:488
msgid "Box size"
msgstr ""

#: src/preferences.c:488
msgid "Select size of box (in number of lines)"
msgstr ""

#: src/preferences.c:492
msgid "lines (0 for no box)"
msgstr "linee (0 per nobox)"

#: src/preferences.c:499
msgid "Printing"
msgstr "Stampa"

#: src/preferences.c:614 src/ui.c:270
#, c-format
msgid ""
"There was an error displaying help: \n"
"%s"
msgstr ""

#: src/preferences.c:717
msgid ""
"The offset format string contains invalid format specifier.\n"
"Only 'x', 'X', 'p', 'P', 'd' and 'o' are allowed."
msgstr ""
"La stringa per il formato della posizione contiene un formato non valido.\n"
"Sono permessi unicamente 'x', 'X', 'p', 'P', 'd' oppure 'o'."

#: src/preferences.c:760
msgid "Can not open desired font!"
msgstr "Impossibile aprire il font desiderato!"

#: src/print.c:57
#, c-format
msgid "Page: %i/%i"
msgstr "Pagina: %i/%i"

#: src/print.c:208 src/print.c:222
#, c-format
msgid ""
"GHex could not find the font \"%s\".\n"
"GHex is unable to print without this font installed."
msgstr ""
"GHex non puo' trovare il carattere \"%s\".\n"
"GHex non puo' stampare senza questo carattere installato."

# SUN CHANGED MESSAGE
#: src/ui.c:66
msgid "Export to _HTML..."
msgstr "Esporta in _HTML..."

#: src/ui.c:70
msgid "Print pre_view..."
msgstr "Anteprima di _stampa..."

#: src/ui.c:83
msgid "Con_verter..."
msgstr "Con_vertitore..."

#: src/ui.c:126
msgid "hex data"
msgstr "dati esadecimali"

#: src/ui.c:127
msgid "ASCII data"
msgstr "dati ASCII"

#: src/ui.c:246
msgid "GHex, a binary file editor"
msgstr "GHex, un editor di file binari"

#: src/ui.c:248
msgid "Released under the terms of GNU Public License"
msgstr "Rilasciato sotto i termini della GNU Public License"

#: src/ui.c:322 src/ui.c:683
msgid "Error saving file!"
msgstr "Errore nel salvataggio del file!"

#: src/ui.c:326 src/ui.c:678
#, c-format
msgid "Saved buffer to file %s"
msgstr "Buffer salvato nel file %s"

#: src/ui.c:350
msgid "Select a file to open"
msgstr "Selezionare un file da aprire"

#: src/ui.c:387
msgid "Select a file to save buffer as"
msgstr "Selezionare un file in cui salvare il buffer"

#: src/ui.c:431
msgid "Select path and file name for the HTML source"
msgstr "Selezionare il percorso e il nome del file per il sorgente HTML"

#: src/ui.c:602
#, c-format
msgid "Really revert file %s?"
msgstr "Davvero ripristinare il file %s?"

#: src/ui.c:616
#, c-format
msgid "Reverted buffer from file %s"
msgstr "Buffer ripristinato dal file %s"

#: src/ui.c:632
#, c-format
msgid "Loaded file %s"
msgstr "Caricato il file %s"

#: src/ui.c:644
msgid "Can not open file!"
msgstr "Impossibile aprire il file!"

#: src/ui.c:687
msgid "Can't open file for writing!"
msgstr "Impossibile aprire il file in scrittura!"

#: src/ui.c:766
msgid "Print Hex Document"
msgstr "Stampa documento esadecimale"

#: src/ui.c:771
msgid "Pages"
msgstr "Pagine"

#: src/ui.c:817
#, c-format
msgid "GHex (%s): Print Preview"
msgstr "GHex: (%s): Anteprima di stampa"

#~ msgid "GHex: Character table"
#~ msgstr "GHex: Tabella caratteri"

#~ msgid "Con_verter"
#~ msgstr "Con_vertitore"

#~ msgid "Help"
#~ msgstr "Aiuto"

#~ msgid "_Settings"
#~ msgstr "_Impostazioni"
